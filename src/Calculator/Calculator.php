<?php declare(strict_types = 1);

namespace Rusted\Tax\Calculator;

class Calculator
{
    // 2019

    /** https://www.timeanddate.com/date/workdays.html?d1=01&m1=01&y1=2020&d2=31&m2=12&y2=2020&ti=on& */
    const YEAR_DAYS_EXCLUDING_WEEKENDS_AND_HOLIDAYS = 254;
    const YEAR_HOLIDAYS = 8;
    const YEAR_EXTRA_HOLIDAYS = 20;
    const HOURS_PER_DAY = 8;
    const IV_DEFAULT_EXPENSES_PERCENT = 30;

    const IV_TAXABLE_INCOME_PERCENT = 15;
    const VSD_TAX_PERCENT = 12.52;
    const PSD_TAX_PERCENT = 6.98;

    const JOB_GPM_PERCENT = 20.0;

    /** https://minfo.lt/verslas/straipsnis/individualios-veiklos-mokesciai-ir-skaiciuokle-2019-metais */
    const SODRA_TAXABLE_PERCENT = 90;

    public function getIVSalaryInfoByHourly(float $hourlySalaryBeforeTax, float $expensesPercent = self::IV_DEFAULT_EXPENSES_PERCENT)
    {
        $yearlySalaryBeforeTax = $hourlySalaryBeforeTax
            * self::HOURS_PER_DAY
            * self::YEAR_DAYS_EXCLUDING_WEEKENDS_AND_HOLIDAYS
        ;

        return $this->getIVSalaryInfoByYearly($yearlySalaryBeforeTax);
    }

    /** https://www.vmi.lt/cms/documents/10174/8274962/Individualios+veiklos+apmokestinimo+tavrka+nuo+2018-01-01/6cb10c13-1f07-4f40-98fc-27c19a2ea768 */
    public function getIVSalaryInfoByYearly(float $yearlySalaryBeforeTax, float $expensesPercent = self::IV_DEFAULT_EXPENSES_PERCENT): array
    {
        $expenses = $yearlySalaryBeforeTax * $expensesPercent / 100;
        $taxableIncome = $yearlySalaryBeforeTax - $expenses;
        $sodraTaxableIncome = $taxableIncome * self::SODRA_TAXABLE_PERCENT / 100;

        if ($yearlySalaryBeforeTax <= 20000) {
            $PKM = $taxableIncome * 0.1;
        } else {
            /** @var 2 pvz https://www.vmi.lt/cms/documents/10174/8274962/Individualios+veiklos+apmokestinimo+tavrka+nuo+2018-01-01/6cb10c13-1f07-4f40-98fc-27c19a2ea768 */
            $PKM = $taxableIncome * (0.1 - ((2 / 300000) * ($taxableIncome - 20000)));
        }

        if ($PKM < 0) {
            $PKM = 0;
        }

        $gpmTax = ($taxableIncome * self::IV_TAXABLE_INCOME_PERCENT / 100) - $PKM;
        $vsdTax = $sodraTaxableIncome * self::VSD_TAX_PERCENT / 100;
        $psdTax =  $sodraTaxableIncome * self::PSD_TAX_PERCENT / 100;
        $yearTaxes = $gpmTax + $vsdTax + $psdTax;
        $yearlySalaryAfterTax = $yearlySalaryBeforeTax - $yearTaxes;

        $dailySalaryAfterTax = round($yearlySalaryAfterTax / self::YEAR_DAYS_EXCLUDING_WEEKENDS_AND_HOLIDAYS, 2);
        $dailySalaryBeforeTax = round($yearlySalaryBeforeTax / self::YEAR_DAYS_EXCLUDING_WEEKENDS_AND_HOLIDAYS, 2);

        $equivalentYearlyJobSalaryAfterTax = $dailySalaryAfterTax * (self::YEAR_DAYS_EXCLUDING_WEEKENDS_AND_HOLIDAYS - self::YEAR_EXTRA_HOLIDAYS);
        $equivalentYearlyJobSalaryBeforeTax = 100 * $equivalentYearlyJobSalaryAfterTax / (100 - self::PSD_TAX_PERCENT - self::VSD_TAX_PERCENT - self::JOB_GPM_PERCENT);

        return [
            'yearlySalaryBeforeTax' => $yearlySalaryBeforeTax,
            'yearlySalaryAfterTax' => $yearlySalaryAfterTax,
            'monthlySalaryBeforeTax' => round($yearlySalaryBeforeTax / 12, 2),
            'monthlySalaryAfterTax' => round($yearlySalaryAfterTax / 12, 2),
            'dailySalaryBeforeTax' => $dailySalaryBeforeTax,
            'dailySalaryAfterTax' => $dailySalaryAfterTax,
            'hourlySalaryBeforeTax' => round($yearlySalaryBeforeTax / self::YEAR_DAYS_EXCLUDING_WEEKENDS_AND_HOLIDAYS / self::HOURS_PER_DAY, 2),
            'hourlySalaryAfterTax' => round($yearlySalaryAfterTax / self::YEAR_DAYS_EXCLUDING_WEEKENDS_AND_HOLIDAYS / self::HOURS_PER_DAY, 2),
            'equivalentYearlyJobSalaryBeforeTax' => round($equivalentYearlyJobSalaryBeforeTax, 2),
            'equivalentMonthlyJobSalaryBeforeTax' => round($equivalentYearlyJobSalaryBeforeTax / 12, 2),
            'equivalentYearlyJobSalaryAfterTax' => round($equivalentYearlyJobSalaryAfterTax, 2),
            'equivalentMonthlyJobSalaryAfterTax' => round($equivalentYearlyJobSalaryAfterTax / 12, 2),
        ];
    }
}