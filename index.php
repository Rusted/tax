<?php

require 'vendor/autoload.php';
use Rusted\Tax\Calculator\Calculator;

$calculator = new Calculator();

for ($i = 1; $i<=200;$i++) {
    $salaries[$i] = $calculator->getIVSalaryInfoByHourly($i);
}

?>
<html>
<head>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table border="1">
    <thead>
        <tr>
            <th colspan="4">Individuali veikla </th>
            <th colspan="4">Samdomas darbas</th>
        </tr>
        <tr>
            <th>
                hour
            </th>
            <th>
                 hour + tax
            </th>
            <th>
                day
            </th>
            <th>
                day + tax
            </th>

            <th>
                month
            </th>
            <th>
                month + tax
            </th>
            <th>
                year
            </th>
            <th>
                year + tax
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($salaries as $hourly => $s): ?>
        <tr <?php if (in_array($hourly, [18, 20, 25, 30, 35, 40, 45, 50])): ?> class="important" <?php endif; ?>>
            <td>
                <?= number_format($salaries[$hourly]['hourlySalaryAfterTax'], 2); ?>
            </td>
            <td>
                <?= number_format($salaries[$hourly]['hourlySalaryBeforeTax'], 2); ?>
            </td>
            <td>
                <?= number_format($salaries[$hourly]['dailySalaryAfterTax'], 2); ?>
            </td>
            <td>
                <?= number_format($salaries[$hourly]['dailySalaryBeforeTax'], 2); ?>
            </td>
            <td>
                <?= number_format($salaries[$hourly]['equivalentMonthlyJobSalaryAfterTax'], 2); ?>
            </td>
            <td>
                <?= number_format($salaries[$hourly]['equivalentMonthlyJobSalaryBeforeTax'], 2); ?>
            </td>
            <td>
                <?= number_format($salaries[$hourly]['equivalentYearlyJobSalaryAfterTax'], 2); ?>
            </td>
            <td>
                <?= number_format($salaries[$hourly]['equivalentYearlyJobSalaryBeforeTax'], 2); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</body>
</html>
