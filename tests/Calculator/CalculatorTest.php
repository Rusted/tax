<?php declare(strict_types = 1);

namespace Rusted\Tax\Tests\Calculator;

use PHPUnit\Framework\TestCase;
use Rusted\Tax\Calculator\Calculator;

class CalculatorTest extends TestCase
{
    public function getIvSalaryInfoByYearlyProvider()
    {
        return [
            [
                10000.0,
                30.0,
                [
                    'yearlySalaryBeforeTax' => 10000.0,
                    /** https://www.tax.lt/skaiciuokles/individualios_veiklos_mokesciu_skaiciuokle */
                    'yearlySalaryAfterTax' => 8421.50,
                    'monthlySalaryBeforeTax' => 833.33,
                    'monthlySalaryAfterTax' => 701.79,
                    'dailySalaryBeforeTax' => 39.37,
                    'dailySalaryAfterTax' => 33.16,
                    'hourlySalaryBeforeTax' => 4.92,
                    'hourlySalaryAfterTax' => 4.14,
                    'equivalentYearlyJobSalaryBeforeTax' => 12825.52,
                    'equivalentMonthlyJobSalaryBeforeTax' => 1068.79,
                    'equivalentYearlyJobSalaryAfterTax' => 7759.44,
                    'equivalentMonthlyJobSalaryAfterTax' => 646.62,
                ]
            ],
            [
                20000.0,
                30.0,
                [
                    'yearlySalaryBeforeTax' => 20000.0,
                    /** https://www.tax.lt/skaiciuokles/individualios_veiklos_mokesciu_skaiciuokle */
                    'yearlySalaryAfterTax' => 16843.0,
                    'monthlySalaryBeforeTax' => 1666.67,
                    'monthlySalaryAfterTax' => 1403.58,
                    'dailySalaryBeforeTax' => 78.74,
                    'dailySalaryAfterTax' => 66.31,
                    'hourlySalaryBeforeTax' => 9.84,
                    'hourlySalaryAfterTax' => 8.29,
                    'equivalentYearlyJobSalaryBeforeTax' => 25647.17,
                    'equivalentMonthlyJobSalaryBeforeTax' => 2137.26,
                    'equivalentYearlyJobSalaryAfterTax' => 15516.54,
                    'equivalentMonthlyJobSalaryAfterTax' => 1293.05
                ]
            ],
            [
                30000.0,
                30.0,
                [
                    'yearlySalaryBeforeTax' => 30000.0,
                    /** https://www.tax.lt/skaiciuokles/individualios_veiklos_mokesciu_skaiciuokle */
                    'yearlySalaryAfterTax' => 25124.5,
                    'monthlySalaryBeforeTax' => 2500.0,
                    'monthlySalaryAfterTax' => 2093.71,
                    'dailySalaryBeforeTax' => 118.11,
                    'dailySalaryAfterTax' => 98.92,
                    'hourlySalaryBeforeTax' => 14.76,
                    'hourlySalaryAfterTax' => 12.36,
                    'equivalentYearlyJobSalaryBeforeTax' => 38259.97,
                    'equivalentMonthlyJobSalaryBeforeTax' => 3188.33,
                    'equivalentYearlyJobSalaryAfterTax' => 23147.28,
                    'equivalentMonthlyJobSalaryAfterTax' => 1928.94
                ]
            ],
        ];
    }

    /** @dataProvider getIvSalaryInfoByYearlyProvider */
    public function testGetIVSalaryInfoByYearly($yearlySalaryBeforeTax, $taxPercent, $expected): void
    {
        $calculator = new Calculator();
        $actual = $calculator->getIVSalaryInfoByYearly($yearlySalaryBeforeTax, $taxPercent);
        $this->assertEquals($expected, $actual);
    }
}
